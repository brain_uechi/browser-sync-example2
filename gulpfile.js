const path = require('path');
const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const env = require('node-env-file');

process.env.BROWSER_SYNC_PROXY = '192.168.99.100';
env(path.resolve('.env-gulp'), {raise: false, overwrite: true});

/**
 * browser-sync task
 */
gulp.task('serve', function (callback) {
  browserSync.init({
    files: [
      'htdocs/**/*.{html,css,js,php}'
    ],
    proxy: process.env.BROWSER_SYNC_PROXY,
    ghostMode: false,
    open: 'external',
  }, () => {
    callback();
  });
});

/**
 * task presets
 */
gulp.task('default', gulp.series('serve'));
